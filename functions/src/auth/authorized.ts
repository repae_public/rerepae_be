import { Request, Response } from "express";
import * as HttpStatus from "http-status-codes";
import * as admin from "firebase-admin";

export function isAuthorized(opts: {
  hasRole: Array<"admin" | "manager" | "user">;
  allowSameUser?: boolean;
}) {
  const firestore = admin.firestore();
  const adminRef = firestore.collection("admins").doc("users");
  return async (req: Request, res: Response, next: Function) => {
    const { role, uid } = res.locals;
    const admins = (await (await adminRef.get()).data()) as { admin: string[] };

    const { id } = req.params;

    if (admins.admin.includes(uid)) return next();

    if (opts.allowSameUser && id && uid === id) return next();

    if (!role) return res.status(HttpStatus.FORBIDDEN).send();

    if (opts.hasRole.includes(role)) return next();

    return res.status(HttpStatus.FORBIDDEN).send();
  };
}
