import { Request, Response } from "express";
import * as HttpStatus from "http-status-codes";
import * as admin from "firebase-admin";

export async function create(req: Request, res: Response) {
  // send email when user is created.
  try {
    const { displayName, password, email, role } = req.body;

    if (!displayName || !password || !email || !role) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ success: false, message: "Missing fields" });
    }

    const { uid } = await admin.auth().createUser({
      displayName,
      password,
      email,
    });
    await admin.auth().setCustomUserClaims(uid, { role });

    return res
      .status(HttpStatus.CREATED)
      .json({ success: true, data: { uid } });
  } catch (err) {
    return handleError(res, err);
  }
}

export async function all(req: Request, res: Response) {
  try {
    const listUsers = await admin.auth().listUsers();
    const users = listUsers.users.map(mapUser);
    return res.status(HttpStatus.OK).json({ success: true, data: users });
  } catch (err) {
    return handleError(res, err);
  }
}

function mapUser(user: admin.auth.UserRecord) {
  const customClaims = (user.customClaims || { roles: "" }) as {
    roles?: string;
  };
  const roles = customClaims.roles ? customClaims.roles : "";
  return {
    uid: user.uid,
    email: user.email || "",
    displayName: user.displayName || "",
    roles,
    lastSignInTime: user.metadata.lastSignInTime,
    creationTime: user.metadata.creationTime,
  };
}

export async function get(req: Request, res: Response) {
  try {
    const { id } = req.params;
    const user = await admin.auth().getUser(id);
    return res
      .status(HttpStatus.OK)
      .json({ success: true, data: mapUser(user) });
  } catch (err) {
    return handleError(res, err);
  }
}

export async function patch(req: Request, res: Response) {
  try {
    const { id } = req.params;
    const { displayName, email, role } = req.body;

    if (!id || !displayName || !email || !role) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ success: false, message: "Missing fields" });
    }

    await admin.auth().updateUser(id, { displayName, email });
    await admin.auth().setCustomUserClaims(id, { role });
    const user = await admin.auth().getUser(id);

    return res
      .status(HttpStatus.NO_CONTENT)
      .json({ success: true, data: mapUser(user) });
  } catch (err) {
    return handleError(res, err);
  }
}

export async function addRole(req: Request, res: Response) {
  try {
    const { id } = req.params;
    const { role } = req.body;

    if (!id || !role) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json({ success: false, message: "Missing fields" });
    }

    const user = await admin.auth().getUser(id);
    const customClaims = (user.customClaims || { roles: [] }).roles || [];
    const roles = customClaims.filter((x: string) => x !== role).concat(role);
    await admin.auth().setCustomUserClaims(id, { roles });

    return res
      .status(HttpStatus.OK)
      .json({ success: true, data: mapUser(await admin.auth().getUser(id)) });
  } catch (err) {
    return handleError(res, err);
  }
}

export async function remove(req: Request, res: Response) {
  try {
    const { id } = req.params;
    await admin.auth().deleteUser(id);
    return res.status(HttpStatus.NO_CONTENT).json({});
  } catch (err) {
    return handleError(res, err);
  }
}

// TODO: factor out to middleware
function handleError(res: Response, err: any) {
  return res
    .status(HttpStatus.INTERNAL_SERVER_ERROR)
    .json({ success: false, message: `${err.code} - ${err.message}` });
}
